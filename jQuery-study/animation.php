<?php include("inc/header.php"); ?>

<?php

function test() {
    echo "Animation End Start Other function";
}
?>

<script>
    $(document).ready(function() {
        $("#animationStart").click(function() {
            $(".animation-img").animate({
                left: '900px',
                opacity: '0.3',
                height: '250px',
                width: '300px',
            }, 20000, function() {
                alert("<?php echo test(); ?>");
            });
        });
        $("#animationStop").click(function() {
            $(".animation-img").stop();
        });
        $("#animationChain").click(function() {
            $(".contain").css("background", "gold").css("color", "gray").slideUp(3000).slideDown(3000);
        });
    });
</script>

<div class="container-fluid">

    <div class="animation-contain">

        <button id="animationStart">Start Animation</button>        
        <button id="animationStop">Stop Animation</button>        


        <img src="img/main-logo.png" alt="Animation" class="animation-img img-responsive"/>       

    </div>
    <button id="animationChain">Slide Animation</button> 
    <div class="contain">           
        <h1>Sliding Effects</h1>
        <h3>Paragraph in here</h3>
        <p>               
            Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
            <br>
            Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
        </p>

    </div>

</div>


<?php include("./inc/footer.php"); ?>