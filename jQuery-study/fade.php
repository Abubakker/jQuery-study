<?php include("inc/header.php"); ?>

<script>
    $(document).ready(function() {
        $("#fadein").click(function() {
            $(".fadeInOutTo").fadeIn('slow');
        });
        $("#fadeout").click(function() {
            $(".fadeInOutTo").fadeOut('slow');
        });
        $("#fadeto").click(function() {
            $(".fadeInOutTo").fadeTo('slow', 0.4);
        });
        $("#fadetoggle").click(function() {
            $(".fadeInOutTo").fadeToggle(2000);
        });
    });
</script>

<div class="container-fluid">

    <div class="fadecon">
        <button id="fadein">Fade In</button>
        <button id="fadeout">Fade Out</button>
        <button id="fadeto">Fade To</button>        
        <button id="fadetoggle">Fade Toggle</button>        

        <div class="fadeInOutTo">           
            <h1>Fade In Out TO Toggle</h1>
            <h3>Paragraph in here</h3>
            <p>               
                Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
                <br>
                Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
            </p>

        </div>

    </div>

</div>


<?php include("./inc/footer.php"); ?>