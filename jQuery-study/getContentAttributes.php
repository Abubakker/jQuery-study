<?php include("inc/header.php"); ?>

<script>
    $(document).ready(function() {

        $("#show").click(function() {
            alert("The Text is : " + $(".contain").text());

        });
        $("#nameInput").click(function() {
            alert("Welcome : " + $("#name").val());

        });
        
        //input fild and show tag / id / class
        
        $("#nameOutput").click(function() {
            $("#showName")+$("#name").val();
        });


    });
</script>

<div class="container-fluid">

    <div class="btn-contain">

        <button id="show">Show Text</button>
        <button id="source">Html Show with code</button>      

        <div class="contain">           
            <h1>Sliding Effects</h1>
            <h3>Paragraph in here</h3>
            <p>
                <strong>Bangladesh</strong>, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
                <br>
                <strong>Bangladesh</strong>, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
            </p>
            <div>
                Name: <input type="text" id="name" placeholder="Type Your Name"/>
                <button id="nameInput">View Your Input</button> 
            </div>
            <div>

                <tr>
                    <td>
                        Name: <input type="text" id="inName" placeholder="Type Your Name"/>
                    </td>
                    <td #showName>
                        Your Name Display here
                    </td>
                    <td>
                        <button id="nameOutput">View Your Input</button> 
                    </td>
                </tr>                

            </div>

        </div>

    </div>

</div>


<?php include("./inc/footer.php"); ?>