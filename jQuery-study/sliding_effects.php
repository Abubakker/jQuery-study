<?php include("inc/header.php"); ?>

<script>
    $(document).ready(function() {
        $("#slideUpbtn").click(function() {
            $(".contain").slideDown('slow');
        });
        $("#slideDownbtn").click(function() {
            $(".contain").slideUp('slow');
        });
        $("#slideTogglebtn").mouseenter(function() {
            $(".contain").slideToggle(2000);
        });
    });
</script>

<div class="container-fluid">

    <div class="btn-contain">
        
        <button id="slideUpbtn">Slide Up</button>
        <button id="slideDownbtn">Slide Down</button>      
        <button id="slideTogglebtn">Slide Toggle</button>        

        <div class="contain">           
            <h1>Sliding Effects</h1>
            <h3>Paragraph in here</h3>
            <p>               
                Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
                <br>
                Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
            </p>

        </div>

    </div>

</div>


<?php include("./inc/footer.php"); ?>