<?php include("inc/header.php"); ?>

<script>
    $(document).ready(function() {
        $("#allContent").click(function() {
            $(".mainContent").hide();
        });
        $("#allH1").dblclick(function() {
            $("h1").hide();
        });
        $("#allParagraph").click(function() {
            $("p").hide();
        });
        $("#mouseEnterHide").mouseenter(function() {
            $("#1stP").hide();
        });
        $("#mouseLeaveHide").mouseleave(function() {
            $("#2ndP").hide();
        });
        $("#hide").click(function() {
            $(".hideShowToggle").hide('slow');
        });
        $("#show").click(function() {
            $(".hideShowToggle").show('slow');
        });
        $("#toggle").click(function() {
            $(".hideShowToggle").toggle('slow');
        });
        $("#toggleh3").click(function() {
            $("h3").toggle('slow');
        });


    });
</script>

<div class="container-fluid">
    

    <div class="mainContent">
        <div class="page">
        <a href="fade.php">FadeIn, FadeOut, FadeTo</a><br/>
        <a href="sliding_effects.php">Sliding Effects</a><br/>
        <a href="animation.php">Animation Method</a><br/>
        <a href="getContentAttributes.php">Get Content and Attributes </a><br/>
        
    </div>


        <h1>Hide Function</h1>
        <h1>Header in here</h1>
        <p>Paragraph in here</p>
        <button id="allContent">Hide All Content and This Div</button>
        <button id="allH1">Hide All H1 Tag. Press Double Click</button>
        <button id="allParagraph">Hide All Paragraph</button>
        <p id="1stP">
            Mouse Enter Hide 1st Paragraph.<br>
            Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.</p>
        <button id="mouseEnterHide">Mouse Enter Hide 1st Paragraph</button>
        <p id="2ndP">
            Mouse Leave Hide 2nd Paragraph.<br>
            Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.</p>
        <button id="mouseLeaveHide">Mouse Leave Hide 2nd Paragraph</button>

        <div class="toggle">
            <button id="hide">Hide</button>
            <button id="show">Show</button>
            <button id="toggle">Toggle</button>
            <button id="toggleh3">Toggle H3</button>
            
            <div class="hideShowToggle">           
                <h1>Hide Show Toggle</h1>
                <h3>Paragraph in here</h3>
                <p>
                    Mouse Enter Hide 1st Paragraph.
                    Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.

                    Mouse Leave Hide 2nd Paragraph.<br>
                    Bangladesh, to the east of India on the Bay of Bengal, is a South Asian country marked by lush greenery and many waterways. Its Padma (Ganges), Meghna and Jamuna rivers create fertile plains, and travel by boat is common. On the southern coast, the Sundarbans, an enormous mangrove forest shared with Eastern India, is home to the royal Bengal tiger.
                </p>



            </div>

        </div>






    </div>


    <?php include("./inc/footer.php"); ?>